# Nginx 文件服务器
*author ：WUYANGHAO*

### 操作步骤
##### 1、下载安装文件
下载<a href="http://nginx.org/download/nginx-1.13.8.tar.gz">Nginx</a>、<a href="https://ftp.pcre.org/pub/pcre/pcre-8.41.tar.gz">Pcre</a>、<a href="http://www.zlib.net/zlib-1.2.11.tar.gz">Zlib</a>、<a href="https://www.openssl.org/source/openssl-1.0.2n.tar.gz">Openssl</a>软件包，与后面安装脚本放在同一目录
##### 2、下载安装脚本
下载<a href="https://gitlab.com/WUYANGHAO/FileserverOfNginx/-/archive/master/FileserverOfNginx-master.zip">install.sh</a>
##### 3、执行安装命令
```bash
bash install.sh
```