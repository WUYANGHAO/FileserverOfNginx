#############################
# author : WUYANGHAO        #
# time : 11:05 2018-01-17   #
#############################
# Offline
#!/bin/bash
function SET_ENV(){
    SCRIPT_PATH=$(cd $(dirname $0);pwd)
    NGINX=$SCRIPT_PATH/$(ls |grep nginx)
    PCRE=$SCRIPT_PATH/$(ls |grep pcre)
    ZLIB=$SCRIPT_PATH/$(ls |grep zlib)
    OPENSSL=$SCRIPT_PATH/$(ls |grep openssl)
    TMP=$SCRIPT_PATH/tmp
    if [ ! -d $TMP ];then
        mkdir $TMP
    fi
}
function GET_SET(){
    read -p "Set the FileServer PATH: " FILE_PATH
    if [ ! -d ${FILE_PATH} ];then
        echo "Error Path: Please create the path folders"
        exit 1
    fi
}
function UN_PKG(){
    tar zxf $NGINX -C $TMP
    tar zxf $PCRE -C $TMP
    tar zxf $ZLIB -C $TMP
    tar zxf $OPENSSL -C $TMP
}
function CONFIG_PKG(){
    cd $TMP/$(ls $TMP|grep nginx)
    ./configure --prefix=/usr/local/nginx --sbin-path=/usr/local/nginx/nginx --conf-path=/usr/local/nginx/nginx.conf --pid-path=/usr/local/nginx/nginx.pid --with-http_ssl_module --with-pcre=../pcre-8.41 --with-zlib=../zlib-1.2.11 --with-openssl=../openssl-1.0.2n
    make
    make install
}
function CREATE_FILECONF(){
    mv /usr/local/nginx/nginx.conf /usr/local/nginx/nginx.conf-bak
cat >/usr/local/nginx/nginx.conf<<EOF
worker_processes  1;
events {
    worker_connections  1024;
}
http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;
    server {
        listen       80;
        charset utf-8;
        server_name  ${LOCAL_IP};
        root    ${FILE_PATH};
        location / {
            autoindex on;
            autoindex_exact_size off;
            autoindex_localtime on;
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}
EOF
    /usr/local/nginx/nginx
    /usr/local/nginx/nginx reload
}
function CLEAR_TMP(){
    rm -rf $TMP
}
function CREATE_UNINSTALL(){
    cat >/usr/local/nginx/uninstall.sh<<EOF
#!/bin/bash
ps -ef |grep nginx|awk '{print $2}'|xargs kill -9
rm -rf /usr/local/nginx
EOF
    chmod +x /usr/local/nginx/uninstall.sh
}
LOCAL_IP=$1
if [ -z ${LOCAL_IP} ];then
    echo "Error Command: bash install.sh <ipaddress>"
    exit 1
else
    ifconfig |grep $LOCAL_IP 1>/dev/null 2>&1
    if [ $? != 0 ];then
        echo "Error Ipaddress: You must enter this machine's ipaddress"
        exit 1
    else
        SET_ENV
        GET_SET
        UN_PKG
        CONFIG_PKG
        CREATE_FILECONF
        CLEAR_TMP
        CREATE_UNINSTALL
    fi
fi